/**
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 10-nov-2013
 */
public class Ejercicio0304 {

  public static void main(String[] args) {
    int a = 5, b = 50, c = 15, mayor, menor;
    
    mayor = menor = a;
    if (b > mayor) {
      mayor = b;
    } else {
      if (b < menor) {
        menor = b;
      }
    }
    
    if (c > mayor) {
      mayor = c;
    } else {
      if (c < menor) {
        menor = c;
      }
    }
    System.out.println("Mayor: " + mayor);
    System.out.println("Menor: " + menor);
  }
}

/* EJECUCION:
 Mayor: 50
 Menor: 5
 */
