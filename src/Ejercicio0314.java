/**
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 18-nov-2013
 */
public class Ejercicio0314 {

  private char letra;

  Ejercicio0314(char let) {
    letra = let;
    if (let >= 'a') {
      letra -= 'a';
      letra += 'A';
    }
  }

  public char getLetra() {
    return letra;
  }

  public void printLetra() {
    System.out.println(letra);
  }

  public static void main(String argv[]) {
    Ejercicio0314 l1 = new Ejercicio0314('a');
    Ejercicio0314 l2 = new Ejercicio0314('A');
    Ejercicio0314 l3 = new Ejercicio0314('b');
    Ejercicio0314 l4 = new Ejercicio0314('g');
    l1.printLetra();
    l2.printLetra();
    System.out.println(l3.getLetra());
    System.out.println(l4.getLetra());
  }
}
/* EJECUCION:
 A
 A
 B
 G
 */
