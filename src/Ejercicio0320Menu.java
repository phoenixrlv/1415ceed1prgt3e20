
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Fichero: Ejercicio0320Menu.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 20-oct-2014
 */
public class Ejercicio0320Menu {

  public static void main(String[] args) {

    int op = 0;
    String buffer;
    InputStreamReader isr = new InputStreamReader(System.in);
    BufferedReader br = new BufferedReader(isr);
    String linea = "";

    System.out.println("Menu");
    System.out.println("0. Salir");
    System.out.println("1. Opcion 1");
    System.out.println("2. Opcion 2");

    System.out.print("Eleguir opción: ");

    try {
      linea = br.readLine();
    } catch (IOException ex) {
      Logger.getLogger(Ejercicio0320Menu.class.getName()).log(Level.SEVERE, null, ex);
    }
    op = Integer.parseInt(linea);

    switch (op) {
      case 0:
        System.out.println("Salir");
        break;
      case 1:
        System.out.println("Opcion 1");
        break;
      case 2:
        System.out.println("Opcion 2");
        break;
      default:
        System.out.println("Opcion Incorrecta");
        break;
    }

  }

}
