import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 06-nov-2013
 */
public class Ejercicio0317 {

  /**
   * @param args the command line arguments
   */
  public static void main(String[] args) {
    // TODO code application logic her

    InputStreamReader input = new InputStreamReader(System.in);
    BufferedReader buffer = new BufferedReader(input);
    int x;
    String linea;
    int suma = 0;

    try {
      do {
        System.out.print("Introduce Numero: ");
        linea = buffer.readLine();
        x = Integer.parseInt(linea);

        if (x != 0) {
          if (x % 2 == 0) {
            suma = suma + x;
          }
        }
      } while (x != 0);
      System.out.println("La suma es: " + suma);

    } catch (IOException ex) {
      System.out.print("Error: " + ex.getMessage());
    }

  }
}

/* Ejecucion
 Introduce Numero: 1
 Introduce Numero: 2
 Introduce Numero: 3
 Introduce Numero: 4
 Introduce Numero: 0
 La suma es: 6
 */
