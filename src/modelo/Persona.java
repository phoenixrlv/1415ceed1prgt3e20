
package modelo;

/**
 * Ejercicio0319
 * @author Paco Aldarias Raya
 * @date 18/10/2013
 */

public class Persona {

  /*
     Clase Persona
     Proporciona los constructores para crear Personas
     */
  
  private String nombre;
  private int edad;

  public Persona() {
    nombre = "";
    edad = 0;
  }

  public String getNombre() {
    return nombre;
  }


  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public int getEdad() {
    return edad;
  }

  public void setEdad(int edad) {
    this.edad = edad;
  }


  
}
