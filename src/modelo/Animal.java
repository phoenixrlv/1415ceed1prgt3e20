
package modelo;

/**
 * Práctica Obligatoria Tema 3
 * @author Paco Aldarias Raya
 * @date 18/10/2013
 */

public class Animal {

  /*
     Clase Animal
     Proporciona los constructores para crear Animal
     */
  
  private String nombre;
  private int edad;

  public Animal() {
    nombre = "";
    edad = 0;
  }

  public String getNombre() {
    return nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public int getEdad() {
    return edad;
  }

  public void setEdad(int edad) {
    this.edad = edad;
  }


  
}
