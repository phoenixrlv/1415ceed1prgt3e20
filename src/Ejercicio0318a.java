/**
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 20-nov-2013
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Ejercicio0318a {

  public static int numeroOpcion(int n) {
    int pos = n;
    if (n >= 1000) {
      pos = 1;
    } else if (n >= 900) {
      pos = 2;
    } else if (n >= 500) {
      pos = 3;
    } else if (n >= 400) {
      pos = 4;
    } else if (n >= 100) {
      pos = 5;
    } else if (n >= 90) {
      pos = 6;
    } else if (n >= 50) {
      pos = 7;
    } else if (n >= 40) {
      pos = 8;
    } else if (n >= 10) {
      pos = 9;
    } else if (n >= 9) {
      pos = 10;
    } else if (n >= 5) {
      pos = 11;
    } else if (n >= 4) {
      pos = 12;
    } else if (n >= 3) {
      pos = 13;
    } else if (n >= 2) {
      pos = 14;
    } else if (n >= 1) {
      pos = 15;
    }
    return pos;
  }

  public static String decimalToRomano(int numdeci) {
    int d = numdeci;
    String s = "";
    while (d > 0) {

      switch (numeroOpcion(d)) {

        case 1:
          d -= 1000;
          s += "M";
          break;
        case 2:
          d -= 900;
          s += "CM";
          break;
        case 3:
          d -= 500;
          s += "D";
          break;
        case 4:
          d -= 400;
          s += "CD";
          break;
        case 5:
          d -= 100;
          s += "C";
          break;
        case 6:
          d -= 90;
          s += "XC";
          break;
        case 7:
          d -= 50;
          s += "L";
          break;
        case 8:
          d -= 40;
          s += "XL";
          break;
        case 9:
          d -= 10;
          s += "X";
          break;
        case 10:
          d -= 9;
          s += "IX";
          break;
        case 11:
          d -= 5;
          s += "V";
          break;
        case 12:
          d -= 4;
          s += "IV";
          break;
        case 13:
          d -= 3;
          s += "III";
          break;
        case 14:
          d -= 2;
          s += "II";
          break;
        case 15:
          d -= 1;
          s += "I";
          break;
      }
    }
    return s;
  }

  public static int pedirNumero() {
    int n = 0;
    InputStreamReader input = new InputStreamReader(System.in);
    BufferedReader buffer = new BufferedReader(input);
    try {
      System.out.print("Introduce numero: ");
      n = (Integer.parseInt(buffer.readLine()));
    } catch (IOException | NumberFormatException e) {
    }
    return n;
  }

  public static void main(String[] args) {
    System.out.println(decimalToRomano(pedirNumero()) + " ");

  }
}
