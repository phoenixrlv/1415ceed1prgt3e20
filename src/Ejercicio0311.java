/**
 * Fichero: Ejercicio0411.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 18-nov-2013
 */
public class Ejercicio0311 {

  public static void main(String[] args) {
    for (int i = 1; i < 101; i++) {
      if (i % 5 > 0) {
        System.out.print(i + " ");
      }
    }
  }
}
