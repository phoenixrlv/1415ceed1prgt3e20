import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Fichero: Ejercicio0320Menu.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 20-oct-2014
 */
public class Ejercicio0320Menu1 {

  public static void main(String[] args) {

    int op = 0;
    String buffer;
    InputStreamReader isr = new InputStreamReader(System.in);
    
    char caracter = ' ';
    String linea;
    
    System.out.println("Menu");
    System.out.println("0. Salir");
    System.out.println("1. Opcion 1");
    System.out.println("2. Opcion 2");

    System.out.print("Eleguir opción: ");

    try {
      caracter = (char) isr.read();
    } catch (IOException ex) {
      Logger.getLogger(Ejercicio0320Menu.class.getName()).log(Level.SEVERE, null, ex);
    }
    
    
    linea = " "+caracter;
    op = Integer.parseInt(linea);

    switch (op) {
      case 0:
        System.out.println("Salir");
        break;
      case 1:
        System.out.println("Opcion 1");
        break;
      case 2:
        System.out.println("Opcion 2");
        break;
      default:
        System.out.println("Opcion Incorrecta");
        break;
    }

  }

}
