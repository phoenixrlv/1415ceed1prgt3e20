/**
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 18-nov-2013
 */
public class Ejercicio0310 {

  public static void main(String[] args) {
    for (int i = 2; i < 101; i += 2) {
      System.out.print(i + " ");
    }
  }
}
