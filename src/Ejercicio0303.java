/**
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 10-nov-2013
 */
public class Ejercicio0303 {

  public static void main(String args[]) {
    boolean seguir = true;
    int contador = 1;
    while (seguir) {
      System.out.print(contador + " ");
      contador++;
      seguir = (contador < 6);
    }
  }

}

/* EJECUCION:
 1 2 3 4 5
 */
