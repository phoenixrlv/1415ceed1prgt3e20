import java.io.BufferedReader;
import java.io.IOException;
import java.lang.NumberFormatException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * @date 27-oct-2014 Fichero Ejercicio0321.java
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 *
 *
 * Programa en java que lee un numero por teclado. Si es numero es menor
 * mostrara error y lo vuelve a leer Usaremos do-while
 *
 */
public class Ejercicio0322LeeNumeroBuclea {

    public static void main(String[] args) throws IOException,NumberFormatException {

        int i = 1;
        String linea = "";

        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);

        do {
            System.out.print("Numero ");

            linea = br.readLine();
            i = Integer.parseInt(linea);

            if (i < 0) {
                System.out.println("Error: Numero < 0");
            }
        } while (i < 0);

        System.out.println("Leido " + i);

    }

}
