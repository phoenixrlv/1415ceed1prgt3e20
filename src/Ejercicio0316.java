/**
 * Fichero: Ejercicio0416.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 18-nov-2013
 */
public class Ejercicio0316 {

  //La funcion devuelve true o false.
  public static boolean esPrimo(int numero) {
    int contador = 2;
    boolean primo = true;
    while ((primo) && (contador < numero)) {
      if (numero % contador == 0) {
        primo = false;
      }
      contador++;
    }
    return primo;
  }

  public static void main(String[] args) {
    int numero = 13;
    boolean resultado = true;
    resultado = esPrimo(numero);
    if (resultado == true) {
      System.out.println("El numero " + numero + " es primo.");
    } else {
      System.out.println("El numero " + numero + " no es primo.");
    }
  }
}
/* EJECUCION:
 El numero 13 es primo.
 * */
