/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vista;

import java.io.IOException;
import modelo.Persona;
import util.*;

/**
 * Ejercicio0319
 * Fichero: VistaPersona.java
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 08-nov-2013
 */

public class VistaPersona {

    public Persona getPersona() throws IOException {

    Persona alumno = new Persona();
    Util util = new Util();
    Vista vista = new Vista();
    String persona;
    int edad = 0;

    System.out.println("TOMA DE DATOS PERSONAS");

    System.out.print("Nombre: ");
    persona = util.pedirString();
    alumno.setNombre(persona);

    while (edad <= 0) {
      System.out.print("Edad: ");
      edad = util.pedirInt();
      if (edad <=0 ) vista.error(2);
    }
    alumno.setEdad(edad);

    return alumno;
  }

  public void showPersona(Persona p1) {

    System.out.println("MOSTRANDO DATOS PERSONAS");
    System.out.println(p1.getNombre() + " " + p1.getEdad());


  }
  
  
}
