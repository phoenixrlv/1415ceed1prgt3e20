package vista;

import util.*;
import modelo.*;
import java.io.IOException;

/**
 * Ejercicio0319
 * Fichero: Vista.java
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 04-nov-2013
 */
public class Vista {

  public int opcion() throws IOException {
    int op;
    Util util;
    util = new Util();

    System.out.println("0. Fin. ");
    System.out.println("1. Persona. ");
    System.out.println("2. Animal. ");
    System.out.print("Opción?: ");
    op = util.pedirInt();
    return op;
  }

  public void error(int i) {
    Errores error = new Errores();
    String t;
    t = error.tipo(i);
    System.out.println("Error: " + t);
  }
}
 