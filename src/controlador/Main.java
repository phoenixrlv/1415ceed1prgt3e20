package controlador;

import java.io.IOException;

import vista.*;
import modelo.*;
import util.*;

/**
 * Ejercicio0319
 * @author Paco Aldarias Raya
 * @date 18/10/2013
 */
public class Main {

  public static void main(String[] args) throws IOException {

    /*
     Clase Main
     Se encargará del control del programa
     */

    int op;
    Util util=new Util();
    Vista vista=new Vista();
    
    do {
    op=vista.opcion();
    switch (op) {
      case 0: 
            util.fin();
            break;
      case 1: 
            Persona p = new Persona();
            VistaPersona vp = new VistaPersona();
            p=vp.getPersona(); 
            vp.showPersona(p); 
            break;
      case 2: 
            Animal a=new Animal();
            VistaAnimal va = new VistaAnimal();
            a=va.getAnimal(); 
            va.showAnimal(a);
            break;
      default:
            vista.error(1);
    }
    } while (op!=0);

  }
}
