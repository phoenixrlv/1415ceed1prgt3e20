/**
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 10-nov-2013
 */
public class Ejercicio0304a {

  public static void main(String args[]) {
    int a = 20, b = 12, c = 14;
    if (a > b && a > c) {
      System.out.println(a + " es mayor que " + b + " y " + c);
    } else if (b > a && b > c) {
      System.out.println(b + " es mayor que " + a + " y " + c);
    } else if (c > a && c > a) {
      System.out.println(c + " es mayor que " + a + " y " + b);
    }
    if (a < b && a < c) {
      System.out.println(a + " es menor que " + b + " y " + c);
    } else if (b < a && b < c) {
      System.out.println(b + " es menor que " + a + " y " + c);
    } else if (c < a && c < a) {
      System.out.println(c + " es menor que " + a + " y " + b);
    }
  }
}
